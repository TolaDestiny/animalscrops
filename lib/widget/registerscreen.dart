import 'package:animal_crops/widget/loginscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool _hiden = true;
  bool _hidenconfirmpassword = true;
  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmpasswordController = TextEditingController();
  String _userregister;


  @override
  void initState() {
    _userregister="";
    super.initState();

  }
  @override
  void dispose() {
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmpasswordController;
    super.dispose();
  }

  void login(){
    setState(() {
      _userregister ="ឈ្មោះ:"+_usernameController.text+"លេខទូរសព៍្ទ"+_emailController.text+"លេខសម្ងាត់:"+_passwordController.text+"បញ្ជាក់លេខសម្ងាត់"+_confirmpasswordController.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 160.0,
                  width: 160.0,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.blue, width: 5.0),
                  ),
                ),
                const SizedBox(height: 50.0),
                TextField(
                  controller: _usernameController,
                  style: const TextStyle(fontSize: 18.0),
                  decoration:const InputDecoration(
                    suffixIcon: Icon(Icons.person),
                    hintText: 'ឈ្មោះ',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 16.0),
                TextField(
                  controller: _emailController,
                  style:const TextStyle(fontSize: 18.0),
                  decoration:const InputDecoration(
                    suffixIcon: Icon(Icons.person),
                    hintText: 'លេខទូរសព័្ទ',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 16.0),
                TextField(
                  controller: _passwordController,
                  obscureText: _hiden,
                  style: const TextStyle(fontSize: 18.0),
                  decoration: InputDecoration(
                    hintText: 'លេខសម្ងាត់',
                    border: const OutlineInputBorder(),
                    suffixIcon: IconButton(
                      onPressed: () {
                        if (_hiden) {
                          setState(() => _hiden = false);
                        } else {
                          setState(() => _hiden = true);
                        }
                      },
                      icon: Icon(
                        _hiden ? Icons.visibility_off : Icons.visibility,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                TextField(
                  controller: _confirmpasswordController,
                  obscureText: _hiden,
                  style: const TextStyle(fontSize: 18.0),
                  decoration: InputDecoration(
                    hintText: 'បញ្ជាក់លេខសម្ងាត់',
                    border: const OutlineInputBorder(),
                    suffixIcon: IconButton(
                      onPressed: () {
                        if (_hidenconfirmpassword) {
                        setState(() => _hidenconfirmpassword = false);
                        } else {
                        setState(() => _hidenconfirmpassword = true);
                        }
                      },
                      icon: Icon(
                        _hidenconfirmpassword ? Icons.visibility_off : Icons.visibility,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                SizedBox(
                  height: 60.0,
                  width: double.infinity,
                  child: CupertinoButton(
                    color: Colors.blue,
                    child: const Text('ចុះឈ្មោះ'),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const LoginPage(),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 16.0),
                Text("Result Register :"+_userregister),
              ],
            ),
          ),
        ),
      ),
    );
  }
}