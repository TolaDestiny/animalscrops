import 'package:animal_crops/widget/homepage.dart';
import 'package:animal_crops/widget/registerscreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _hiden = true;

  Future<void> _userLogin(String email, String password) async {
    final _auth = FirebaseAuth.instance;
    String errorMessage = '';
    try {
      await _auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((uid) async => {
        _buildLoading(),
        await Future.delayed(const Duration(seconds: 2)),
        Navigator.pushAndRemoveUntil(
          (context),
          MaterialPageRoute(builder: (context) => const HomePage()),
              (route) => false,
        ),
      });
    } on FirebaseAuthException catch (error) {
      switch (error.code) {
        case "invalid-email":
          errorMessage = "Your email address appears to be malformed.";
          break;
        case "wrong-password":
          errorMessage = "Your password is wrong.";
          break;
        case "user-not-found":
          errorMessage = "User with this email doesn't exist.";
          break;
        case "user-disabled":
          errorMessage = "User with this email has been disabled.";
          break;
        case "too-many-requests":
          errorMessage = "Too many requests";
          break;
        case "operation-not-allowed":
          errorMessage = "Signing in with Email and Password is not enabled.";
          break;
        default:
          errorMessage = "An undefined Error happened.";
      }
      Fluttertoast.showToast(msg: errorMessage);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 160.0,
                  width: 160.0,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.green, width: 5.0),
                  ),
                ),
                const SizedBox(height: 16.0,),
                Container(
                    child: Text("ចូលគណនី",
                      style: const TextStyle(fontSize: 18.0),),
                ),
                const SizedBox(height: 16.0,),
                const TextField(
                  style: TextStyle(fontSize: 18.0),
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.person),
                    hintText: 'លេខទូរសព័្ទ',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 16.0),
                TextField(
                  obscureText: _hiden,
                  style: const TextStyle(fontSize: 18.0),
                  decoration: InputDecoration(
                    hintText: 'លេខសម្ងាត់',
                    border: const OutlineInputBorder(),
                    suffixIcon: IconButton(
                      onPressed: () {
                        if (_hiden) {
                          setState(() => _hiden = false);
                        } else {
                          setState(() => _hiden = true);
                        }
                      },
                      icon: Icon(
                        _hiden ? Icons.visibility_off : Icons.visibility,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 5.0),
                Container(
                  margin: const EdgeInsets.only(left: 246.0),
                  child: TextButton(
                    child: const Text('ភ្លេចពាក្យសម្ងាត់?'),
                    onPressed: () {},
                  ),
                  // const Text('forget password?'),
                ),
                const SizedBox(height: 6.0),
                SizedBox(
                  height: 60.0,
                  width: double.infinity,
                  child: CupertinoButton(
                    color: Colors.green,
                    child: const Text('ចូល',
                      style: const TextStyle(fontSize: 18.0),),
                    onPressed: () {},
                  ),
                ),
                const SizedBox(height: 16.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text('បង្កើតគណនី?',
                      style: const TextStyle(fontSize: 18.0),),
                    TextButton(
                      onPressed: () {
                        // Navigator.pop(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const RegisterScreen()),);
                      },
                      child: const Text('ចុះឈ្មោះ',
                        style: const TextStyle(
                            fontSize: 18.0),),
                    )
                  ],
                ),
                // Container(
                //   height: 200,
                //   color: Colors.green,
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
Widget _buildLoading() {
  return const Scaffold(
    body: Center(
      child: CircularProgressIndicator(),
    ),
  );
}